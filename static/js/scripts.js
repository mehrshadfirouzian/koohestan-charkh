$(document).ready(function(){
    $("#brand-search").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#brands label").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
});
function parseURLParams(url) {
    var queryStart = url.indexOf("?") + 1,
        queryEnd   = url.indexOf("#") + 1 || url.length + 1,
        query = url.slice(queryStart, queryEnd - 1),
        pairs = query.replace(/\+/g, " ").split("&"),
        parms = {}, i, n, v, nv;

    if (query === url || query === "") return;

    for (i = 0; i < pairs.length; i++) {
        nv = pairs[i].split("=", 2);
        n = decodeURIComponent(nv[0]);
        v = decodeURIComponent(nv[1]);

        if (!parms.hasOwnProperty(n)) parms[n] = [];
        parms[n].push(nv.length === 2 ? v : null);
    }
    return parms;
}
$("input[type='checkbox']").change(function (){
    $("#filter-form").submit();
});
$(document).ready(function (){
    var ids = Object.keys(parseURLParams(window.location.href));
    console.log(parseURLParams(window.location.href));
    for (i=0;i<=ids.length;i++){
        if (ids[i]=="sort-by"){
            $("#sort-by").val(parseURLParams(window.location.href)[ids[i]]);
            continue;
        }
        var elm = $("input[name='"+ids[i]+"']");
        if (elm.length){
            switch(elm.attr("type")){
                case "checkbox":
                    elm.prop("checked", true);
                    break;
                case "number":
                    elm.val(parseURLParams(window.location.href)[ids[i]]);
                    break;
            }
        }
    }

});