from django.db import models
from django_countries.fields import CountryField

class Brand(models.Model):
    name = models.CharField(max_length=20, verbose_name='نام برند')
    origin_country = CountryField(verbose_name='کشور تولیدکننده')
    description = models.TextField(blank=True, null=True, verbose_name='توضیحات')
    website = models.URLField(blank=True, null=True, verbose_name='وبسایت تولیدکننده')
    bicycle_company = models.BooleanField("تولیدکننده دوچرخه", default=False)

    def __str__(self):
        return self.name

class ItemImage(models.Model):
    description = models.CharField(max_length=30, verbose_name='توضیحات')
    image = models.ImageField(upload_to='item-images/', verbose_name='عکس')

    def __str__(self):
        return self.description

class Category(models.Model):
    name = models.CharField(max_length=15, verbose_name='عنوان')

    description = models.TextField(verbose_name='توضیحات', null=True, blank=True)

    for_bike = models.BooleanField(default=False, verbose_name='برای دوچرخه است')
    for_accessory = models.BooleanField(default=False, verbose_name='برای لوازم جانبی است')

    image = models.ForeignKey(ItemImage, models.CASCADE, null=True, blank=True, verbose_name='عکس')

    def __str__(self):
        return self.name

class Component(models.Model):
    name = models.CharField(max_length=30, verbose_name='نام قطعه')
    brand = models.ForeignKey(Brand, models.SET_NULL, blank=True, null=True, verbose_name='شرکت تولیدکننده')
    category = models.ForeignKey(Category, models.CASCADE, verbose_name='نوع قطعه', limit_choices_to={'for_bike': False, 'for_accessory': False})
    price = models.BigIntegerField(verbose_name='قیمت')
    inventory_amount = models.PositiveIntegerField(verbose_name='موجودی انبار')
    last_update_date = models.DateField(auto_now=True)
    images = models.ManyToManyField(ItemImage, blank=True)

    def __str__(self):
        return self.name + ' ' +self.brand.name

    def formatted_price(self):
        return '{:,}'.format(self.price)

class BikeSize(models.Model):
    size = models.CharField(max_length=10, verbose_name='سایز')

    def __str__(self):
        return self.size

class BikeColor(models.Model):
    color = models.CharField(max_length=20, verbose_name='رنگ')

    def __str__(self):
        return self.color

class Accessory(models.Model):
    name = models.CharField(max_length=35, verbose_name='نام')
    category = models.ForeignKey(Category, models.CASCADE, verbose_name='دسته بندی', limit_choices_to={'for_accessory': True}, null=True, blank=True)
    company = models.ForeignKey(Brand, models.SET_NULL, null=True, blank=True, verbose_name='شرکت سازنده')
    is_fake = models.BooleanField(default=True, verbose_name='تقلبی')
    images = models.ManyToManyField(ItemImage, verbose_name='عکس ها', blank=True)
    price = models.BigIntegerField('قیمت')

    def __str__(self):
        if self.company:
            return self.name + ' ' + self.company.name
        return self.name

    def formatted_price(self):
        return '{:,}'.format(self.price)

class Bicycle(models.Model):
    brand = models.ForeignKey(Brand, models.CASCADE, verbose_name='برند')
    model_name = models.CharField(max_length=30, verbose_name='مدل دوچرخه')
    sizes = models.ForeignKey(BikeSize, models.CASCADE, verbose_name='سایز ها')
    color = models.ManyToManyField(BikeColor, verbose_name='رنگ ها')
    category = models.ManyToManyField(Category, verbose_name='نوع', limit_choices_to={'for_bike': True})
    price = models.BigIntegerField(verbose_name='قیمت')
    inventory_amount = models.PositiveIntegerField(verbose_name='موجودی در انبار')
    last_update_date = models.DateField(auto_now=True)

    BIKE_SYSTEM_CHOICES = [
        ('WG', 'دنده ای'),
        ('WOG', 'ساده')  # W/O gear
    ]
    bike_type = models.CharField(max_length=5, choices=BIKE_SYSTEM_CHOICES, verbose_name='سیستم دوچرخه')

    BRAKE_CHOICES = [
        ('disk', 'دیسکی'),
        ('rim', 'لقمه ای')
    ]
    brake_type = models.CharField(max_length=10, choices=BRAKE_CHOICES, verbose_name='نوع ترمز')
    
    images = models.ManyToManyField(ItemImage, blank=True, verbose_name='عکس ها')
    description = models.TextField(blank=True, null=True, verbose_name='توضیحات')

    weight = models.PositiveIntegerField(verbose_name='وزن', blank=True, null=True)
    gear_amount = models.PositiveIntegerField(null=True, blank=True, verbose_name='تعداد دنده')

    accessories = models.ManyToManyField(Accessory, blank=True, verbose_name='اقلام همراه')

    # Technical components
    fork = models.ForeignKey(Component, models.SET_NULL, null=True, blank=True, verbose_name='دوشاخ', related_name='bike_fork', limit_choices_to={'categories__name__contains': 'دوشاخ'})
    shock = models.ForeignKey(Component, models.SET_NULL, null=True, blank=True, verbose_name='فنر وسط', related_name='bike_shock', limit_choices_to={'categories__name__contains': 'فنر وسط'})
    handlebar = models.ForeignKey(Component, models.SET_NULL, null=True, blank=True, verbose_name='دسته فرمان', related_name='bike_handlebar', limit_choices_to={'categories__name__contains': 'دسته فرمان'})
    stem = models.ForeignKey(Component, models.SET_NULL, null=True, blank=True, verbose_name='کرپی فرمان', related_name='bike_stem', limit_choices_to={'categories__name__contains': 'کرپی فرمان'})
    seatpost = models.ForeignKey(Component, models.SET_NULL, null=True, blank=True, verbose_name='لوله زین', related_name='bike_seatpost', limit_choices_to={'categories__name__contains': 'لوله زین'})
    saddle = models.ForeignKey(Component, models.SET_NULL, null=True, blank=True, verbose_name='زین', related_name='bike_saddle', limit_choices_to={'categories__name__contains': 'زین'})
    pedals = models.ForeignKey(Component, models.SET_NULL, null=True, blank=True, verbose_name='رکاب', related_name='bike_pedals', limit_choices_to={'categories__name__contains': 'رکاب'})
    shifters = models.ForeignKey(Component, models.SET_NULL, null=True, blank=True, verbose_name='دسته دنده', related_name='bike_shifters', limit_choices_to={'categories__name__contains': 'دسته دنده'})
    front_derailleur = models.ForeignKey(Component, models.SET_NULL, null=True, blank=True, verbose_name='طبق عوض کن', related_name='bike_front_derailleur', limit_choices_to={'categories__name__contains': 'طبق عوض کن'})
    rear_derailleur = models.ForeignKey(Component, models.SET_NULL, null=True, blank=True, verbose_name='شانژمان', related_name='bike_rear_derailleur', limit_choices_to={'categories__name__contains': 'شانژمان'})
    brakes = models.ForeignKey(Component, models.SET_NULL, null=True, blank=True, verbose_name='ترمز', related_name='bike_brakes', limit_choices_to={'categories__name__contains': 'ترمز'})
    cassette = models.ForeignKey(Component, models.SET_NULL, null=True, blank=True, verbose_name='خودرو', related_name='bike_cassette', limit_choices_to={'categories__name__contains': 'خودرو'})
    chain = models.ForeignKey(Component, models.SET_NULL, null=True, blank=True, verbose_name='زنجیر', related_name='bike_chain', limit_choices_to={'categories__name__contains': 'زنجیر'})
    crankset = models.ForeignKey(Component, models.SET_NULL, null=True, blank=True, verbose_name='طبق قامه', related_name='bike_crankset', limit_choices_to={'categories__name__contains': 'طبق قامه'})
    rims = models.ForeignKey(Component, models.SET_NULL, null=True, blank=True, verbose_name='طوقه', related_name='bike_rims', limit_choices_to={'categories__name__contains': 'طوقه'})
    hubs = models.ForeignKey(Component, models.SET_NULL, null=True, blank=True, verbose_name='توپی', related_name='bike_hubs', limit_choices_to={'categories__name__contains': 'توپی'})
    tires = models.ForeignKey(Component, models.SET_NULL, null=True, blank=True, verbose_name='تایر', related_name='bike_tires', limit_choices_to={'categories__name__contains': 'تایر'})

    def __str__(self):
        return self.brand.name + ' ' + self.model_name

    @property
    def formatted_price(self):
        return '{:,}'.format(self.price)

    def formatted_categories(self):
        return '، '.join([i.name for i in self.category.all()])
    
    def formatted_accessories(self):
        accessories = ['<a href="/accessory/{}">'.format(i.id) + i.name + '</a>' for i in self.accessories.all()]
        return '، '.join(accessories) if accessories else 'اقلامی همراه این دوچرخه موجود نمی‌باشد'