from django.conf.urls.static import static
from django.urls import path
from django.conf import settings

from . import views


urlpatterns = [
    path('', views.index, name='index'),
    path('bicycle/<int:pk>', views.bicycle),
    path('accessories/select-category', views.accessory_categories),
    path('accessories/<int:pk>', views.accessory),
    path('accessory/<int:pk>', views.single_accessory),
    path('components/select-category', views.component_categories),
    path('components/<int:pk>', views.components_list),
    path('component/<int:pk>', views.single_component)
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
