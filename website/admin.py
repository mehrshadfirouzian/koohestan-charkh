from django.contrib import admin
from .models import Brand, ItemImage, Category, Component, BikeSize, BikeColor, Bicycle, Accessory

admin.site.register(Bicycle)
admin.site.register(Brand)
admin.site.register(ItemImage)
admin.site.register(Category)
admin.site.register(Component)
admin.site.register(BikeColor)
admin.site.register(BikeSize)   
admin.site.register(Accessory)