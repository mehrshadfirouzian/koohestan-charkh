from django.shortcuts import render
from django.core.paginator import Paginator

from .models import Bicycle, Brand, BikeColor, BikeSize, Category, Accessory, Component

import re

def index(request):
    filters = list(request.GET.keys())
    brands = list(filter(lambda f: re.compile('^brand-\d+').match(f), filters))
    brands = list(map(lambda x: int(x[x.find('-')+1:]), brands))

    colors = list(filter(lambda f: re.compile('^color-\d+').match(f), filters))
    colors = list(map(lambda x: int(x[x.find('-')+1:]), colors))

    gears = list(filter(lambda f: re.compile('^gear-\d+').match(f), filters))
    gears = list(map(lambda x: int(x[x.find('-')+1:]), gears))

    sizes = list(filter(lambda f: re.compile('^size-\d+').match(f), filters))
    sizes = list(map(lambda x: int(x[x.find('-')+1:]), sizes))

    types = list(filter(lambda f: re.compile('^type-\d+').match(f), filters))
    types = list(map(lambda x: int(x[x.find('-')+1:]), types))

    bicycles = Bicycle.objects.all()

    if brands:
        bicycles = bicycles.filter(brand__pk__in=brands)
    
    if colors:
        bicycles = bicycles.filter(color__pk__in=colors)
    
    if gears:
        bicycles = bicycles.filter(gear_amount__in=gears)

    if sizes:
        bicycles = bicycles.filter(sizes__size__in=sizes)

    if types:
        bicycles = bicycles.filter(type__pk__in=types)

    if request.GET.get('min'):
        bicycles = bicycles.filter(price__gte=request.GET.get('min'))

    if request.GET.get('max'):
        bicycles = bicycles.filter(price__lte=request.GET.get('max'))

    if request.GET.get('sort-by'):
        by = request.GET.get('sort-by')
        if by=='high-low':
            bicycles = bicycles.order_by('-price')
        elif by=='low-high':
            bicycles = bicycles.order_by('price')
        else:
            pass


    paginator = Paginator(bicycles, 24)

    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    return render(request, 'index.htm', {
        'bikes': page_obj, 
        'brands': Brand.objects.filter(bicycle_company=True),
        'colors': BikeColor.objects.all(),
        'sizes': BikeSize.objects.all(),
        'types': Category.objects.filter(for_bike=True),
        'gears': [21, 24, 27, 28, 30]
    })

def bicycle(request, pk):
    bike_object = Bicycle.objects.get(pk=pk)

    return render(request, 'bicycle.htm', {'bike': bike_object})

def accessory_categories(request):
    return render(request, 'accessory-cats.htm', {'categories': Category.objects.filter(for_accessory=True)})

def accessory(request, pk):
    accessories = Accessory.objects.filter(category__id=pk)

    if request.GET.get('min'):
        accessories = accessories.filter(price__gte=request.GET.get('min'))

    if request.GET.get('max'):
        accessories = accessories.filter(price__lte=request.GET.get('max'))

    if request.GET.get('sort-by'):
        by = request.GET.get('sort-by')
        if by=='high-low':
            accessories = accessories.order_by('-price')
        elif by=='low-high':
            accessories = accessories.order_by('price')
        else:
            pass

    return render(request, 'accessories.htm', {'accessories': accessories})

def single_accessory(request, pk):
    return render(request, 'accessory.htm', {'accessory': Accessory.objects.get(pk=pk)})

def component_categories(request):
    return render(request, 'component-cats.htm', {'categories': Category.objects.filter(for_bike=False, for_accessory=False)})

def components_list(request, pk):
    components = Component.objects.filter(category__id=pk)

    if request.GET.get('min'):
        components = components.filter(price__gte=request.GET.get('min'))

    if request.GET.get('max'):
        components = components.filter(price__lte=request.GET.get('max'))

    if request.GET.get('sort-by'):
        by = request.GET.get('sort-by')
        if by=='high-low':
            components = components.order_by('-price')
        elif by=='low-high':
            components = components.order_by('price')
        else:
            pass

    return render(request, 'components.htm', {'components': components})

def single_component(request, pk):
    return render(request, 'component.htm', {'component': Component.objects.get(pk=pk)})